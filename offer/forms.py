#coding: utf-8

from django.db import models
from client.models import *
#from performer.models import *

from datetime import datetime

OFFER_STAT = (
	(1, u'Одобрено'),
	(0, u'Отказано'),
)

# Create your models here.
class Offer(models.Model):
	Offer_nom = models.CharField(u'Номер заявки', max_length=10)
	#customer_id = models.ForeignKey(u'Заказчик', 'Customer')
        #performer_id = models.ForeignKey(u'Исполнитель', 'Performer')
	status = models.IntegerField(u'Решение по МП', choices=OFFER_STAT)
	date = models.DateTimeField(u'Дата решения', default=datetime.now)

	class Meta:
		verbose_name = u'Предложение'
		verbose_name_plural = u'Предложения'

class OfferForm(models.Model):
	offer_nom = models.CharField(u'Номер предложения', max_length=10)
	#customer_id = models.ForeignKey(u'Заказчик', 'Customer')
        #performer_id = models.ForeignKey(u'Исполнитель', 'Performer')
	status = models.IntegerField(u'Решение по МП', choices=OFFER_STAT)
	date = models.DateTimeField(u'Дата решения', default=datetime.now)

	class Meta:
		verbose_name = u'Предложение'
		verbose_name_plural = u'Предложения'

#class Service():
#	name = models.CharField(u'Наименование', max_length=10)
