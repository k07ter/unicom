#coding: utf-8

from django.db import models
from client.models import *
#from performer.models import *

from datetime import datetime

OFFER_STAT = (
	(1, u'Одобрено'),
	(0, u'Отказано'),
)
OFFER_TYPE = [u'Потреб', u'Ипотека', u'Кредит', u'КМСБ']

# Create your models here.
class Offer(models.Model):

	dateCreate = models.DateTimeField(u'Дата и время создания:', default=datetime.now)
	dateChange = models.DateTimeField(u'Дата и время изменения:', default=datetime.now)
	dateRotationStart = models.DateTimeField(u'Дата и время начала ротации:', default=datetime.now)
	dateRotationFinish = models.DateTimeField(u'Дата и время окончания ротации:', default=datetime.now)

	offerName = models.CharField(u'Название предложения:', max_length=100)
	offerType = models.IntegerField(u'Тип предложения:', choices=OFFER_TYPE)

	maxScoreMark = models.FloatField(u'Максимальный скорринговый балл:', max_length=3)
	minScoreMark = models.FloatField(u'Минимальный скорринговый балл:', max_length=3)

	credOrg = models.ForeignKey(u'Кредитная организация', 'Client')

	class Meta:
		db_table = 'offers'
		verbose_name = u'Предложение'
		verbose_name_plural = u'Предложения'

