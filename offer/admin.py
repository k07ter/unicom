#coding: utf-8

from django.contrib import admin
from models import Offer

# Register your models here.
class OfferAdmin(admin.ModelAdmin):
	def __unicode__(self):
		return 'offer'
		
	class Meta:
		model = Offer
		verbose_name = u'Предложение'
		verbose_name_plural = u'Предложения'

admin.site.register(Offer, OfferAdmin)

