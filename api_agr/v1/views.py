﻿from django.contrib.auth.models import User, Group
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse
from rest_framework.response import Response

from .serializers import UserSerializer, ClientSerializer, PartnerSerializer, IssueSerializer

@api_view(['GET'])
def api_agr(request, format=None):
    """
    The entry endpoint of our API.
    """
    return Response({
        'users': reverse('user-list', request=request),
        #'groups': reverse('group-list', request=request),
        #'clients': reverse('client-list', request=request),
        #'partners': reverse('partner-list', request=request),
        #'issues': reverse('issue-list', request=request),
    })

class UserList(generics.ListCreateAPIView):
    """
    API endpoint that represents a list of users.
    """
    model = User
    serializer_class = UserSerializer

class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that represents a single user.
    """
    model = User
    serializer_class = UserSerializer


class GroupList(generics.ListCreateAPIView):
    """
    API endpoint that represents a list of groups.
    """
    model = Group
    serializer_class = GroupSerializer

class GroupDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that represents a single group.
    """
    model = Group
    serializer_class = ClientSerializer

'''
class ClientList(generics.ListCreateAPIView):
    """
    API endpoint that represents a list of groups.
    """
    model = Client
    serializer_class = ClientSerializer

class ClientDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that represents a single group.
    """
    model = Client
    serializer_class = ClientSerializer

class PartnerList(generics.ListCreateAPIView):
    """
    API endpoint that represents a list of groups.
    """
    model = Partner
    serializer_class = PartnerSerializer

class PartnerDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that represents a single group.
    """
    model = Partner
    serializer_class = PartnerSerializer
    
class IssueList(generics.ListCreateAPIView):
    """
    API endpoint that represents a list of groups.
    """
    model = Issue
    serializer_class = IssueSerializer

class IssueDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that represents a single group.
    """
    model = Issue
    serializer_class = IssueSerializer
'''
