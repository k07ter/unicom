#coding: utf-8
from django.db import models

# Create your models here.
"""
class Api_AgrBase(models.Model):

	abstract_class = True

	def __init__(self, mdl, serial_class):
		models = mdl
		serialize_class = serial_class
"""

class Api_Agr(models.Model):

	ACTIONS = (
 		('get', u'СОЗДАТЬ'),
		('post', u'ПОСМОТРЕТЬ'),
		('put', u'ИЗМЕНИТЬ'),
		('delete', u'УДАЛИТЬ')
	)

 	#title = models.CharField(u'Команда', max_length=255)
	description = models.CharField(u'Описание', max_length=750)
	event = models.CharField(u'Действие', choices=ACTIONS, max_length=12)
	model_tip = models.CharField(u'Иформация о:', choices=(('Client', u'Анкета'), ('Issue', u'Заявка')), max_length=10)
	view_mode = models.CharField(u'Режим вывода', choices=(('Detail', u'Один'), ('List', u'Список')), max_length=8)
	status = models.BooleanField(u'Статус заявки', choices=((False,'Новая'),(True,'Просмотрена')), default=False)
	cmd = models.IntegerField(u'Команда', default=0)

	#def build_command(self):
 	#	self.cmd = 'self.event + self.model_tip + self.view_mode'
	#	#self.save()

        class Meta:
                db_table = 'api_agr'
                verbose_name = u'API запрос'
                verbose_name_plural = u'API запросы'

        def __unicode__(self):
                return u'%s' % self.description

	"""
	class ApiAgrDetailView(models.Model):

	def get(self, request): pass

	def post(self, request): pass
	
	def put(self, request): pass
	
	def delete(self, request) :pass

	class ApiAgrListView(models.Model):
	"""
	def get(self, request, cls):
		return Cls.objects.all()

	def post(self, request, cls):
		print(Cls.objects.all())

