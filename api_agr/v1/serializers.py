from django.contrib.auth.models import User, Group, Permission
from rest_framework import serializers

from unicom.models import Partner
from client.models import Client, Issue, Offer

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')

"""
class GroupSerializer(serializers.HyperlinkedModelSerializer):
    permissions = serializers.ManySlugRelatedField(
        #slug_field='codename',
        queryset=Permission.objects.all()
    )

    class Meta:
        model = Group
        #fields = ('url', 'name', 'permissions')

class ClientSerializer(serializers.HyperlinkedModelSerializer):
    #permissions = serializers.ManySlugRelatedField(
    #    slug_field='codename',
    #    queryset=Permission.objects.all()
    #)

    class Meta:
        model = Client
        #fields = ('url', 'name', 'permissions')

class PartnerSerializer(serializers.HyperlinkedModelSerializer):
    #permissions = serializers.ManySlugRelatedField(
    #    slug_field='codename',
    #    queryset=Permission.objects.all()
    #)

    class Meta:
        model = Partner
        #fields = ('url', 'name', 'permissions')

class IssueSerializer(serializers.HyperlinkedModelSerializer):
    #permissions = serializers.ManySlugRelatedField(
    #    slug_field='codename',
    #    queryset=Permission.objects.all()
    #)

    class Meta:
        model = Issue
        #fields = ('url', 'name', 'permissions')
"""

from .models import Api_Agr
class Api_AgrSerializer(serializers.ModelSerializer):

    #permissions = serializers.ManySlugRelatedField(
    #    slug_field='codename',
    #    queryset=Permission.objects.all()
    #)

    class Meta:
            model = Api_Agr

