from rest_framework.serializers import ModelSerializer
from .models import Api_Agr

class Api_AgrSerializer(ModelSerializer):

    class Meta:
            model = Api_Agr

