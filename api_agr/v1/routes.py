from rest_framework import routers
from .viewsets import * #Api_AgrViewSet
from rest_framework.urlpatterns import format_suffix_patterns

api_router = routers.SimpleRouter() #+ format_suffix_patterns([], allowed=['json', 'html'])
api_router.register('Api_Agr', Api_AgrViewSet + format_suffix_patterns([], allowed=['json', 'html']))
#api_router.register('Api_Agr/issue', IssueViewSet)
