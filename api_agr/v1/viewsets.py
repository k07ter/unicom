from rest_framework import viewsets
from .models import Api_Agr
from .serializers import Api_AgrSerializer

class Api_AgrViewSet(viewsets.ModelViewSet):
    queryset = Api_Agr.objects.all()
    serializer_class = Api_AgrSerializer

