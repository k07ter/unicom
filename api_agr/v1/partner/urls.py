from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'current.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #url(r'^dj/api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'/cred_org/', include(admin.site.urls)),
    url(r'/part/', include(admin.site.urls)),
)
