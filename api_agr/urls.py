from django.conf.urls import patterns, include, url

from django.contrib import admin
from api_agr.v1.routes import api_router
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    #url(r'^$', 'api_list', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', api_router),
    #include('rest_framework.urls', namespace='rest_framework')),
    #url(r'^cred_org$', 'issue'),
    #url(r'^cred_org/(/d+)$', 'issue_list'),
    #url(r'^partner$', 'offer'),
    #url(r'^partner/(\d+)$', 'offer_list'),
)
