#coding: utf-8

from django.contrib import admin
from api_agr.v1.models import Api_Agr

# Register your models here.
class Api_AgrAdmin(admin.ModelAdmin):

	list_display = ['description','event','model_tip','view_mode']	
	
	fieldsets = (
		(u'Параметры запроса',
			{'fields': (('description','event','model_tip','view_mode'),)
			}),
	)
	class Meta:
		model = Api_Agr
		verbose_name = u'API запрос'
		verbose_name_plural = u'API запросы'

admin.site.register(Api_Agr, Api_AgrAdmin)

