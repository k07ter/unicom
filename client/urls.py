from django.conf.urls import patterns, include, url
from django.contrib import admin
#from . import views #import home, cust, inv

urlpatterns = patterns('client.views',
    # Examples:
    url(r'^$', 'home', name='home'),
    #url(r'^inv/$', views.inv, name='inv'),
    #url(r'^inv/(\d+)/$', views.inv, name='inv'),
    #url(r'^spr/(\d+)/$', views.spr, name='spr'),
    url(r'^(\d+)/$', 'clnt', name='clnt'),
)
