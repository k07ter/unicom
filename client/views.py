#coding: utf-8

import os
from django.shortcuts import render, render_to_response
from models import Client
#from unicom.models import Jobkind

# Create your views here.
def home(request):
	#curr = Site.objects.get_current().domain
	#all_cust = [] #{'name': u'Миша'}, {'name': u'Петя'}]
	clnt_db = Client.objects #filter(name=request.subdomain)  #using('customer').all()
	#print(clnt_db)
	return render_to_response('client/index.html', {'clients': clnt_db}) #, 'index.html')

def clnt(request, arg):
	print(arg)
	#kat = 'customer' #os.getcwd().split('/')[-1]
	#all_cust [] #{'name': u'Аня', 'nom': 2}, {'name': u'Маня', 'nom':3}]
	clnt = Client.objects.filter(id=arg) #using('customer').get(nom=arg)
	if clnt:
		print(clnt)
	else:
		print(u'Нету')
		#Client.objects.create(fam='a',name='b',otch='c',attestat_nomer='000',custid=arg ,ogrn='qwe123')

	return render_to_response('client/clnt.html', {'clnt': clnt})

#def spr(request, arg):
	#print(arg)
	#kat = 'customer' #os.getcwd().split('/')[-1]
	#all_cust [] #{'name': u'Аня', 'nom': 2}, {'name': u'Маня', 'nom':3}]
	#spr = Jobkind.objects.filter(id=arg) #using('customer').get(nom=arg)
	#if spr:
	#	print(spr)
	#else:
	#	print(u'Нету')
	#	Jobkind.objects.create(fam='a',name='b',otch='c',attestat_nomer='000',clntid=arg ,ogrn='qwe123')
	#return render_to_response('client/clnt.html', {'clnt': clnt})

#def inv(request, arg=None):
#	if arg:
#		print(arg)

	#cust_sb = Customer.objects.all()
#	print('Invite')
#	apl = 'client' #os.getcwd().split('/')[-1]
#	return render_to_response('client/invite.html')
