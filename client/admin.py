#coding: utf-8

from django.contrib import admin
from models import Client, Issue, Offer
#from issue import Issue
#from offer import Offer

# Register your models here.
class ClientAdmin(admin.ModelAdmin):

	#app_label = ('Client', 'Gate')
	list_display = ['id', 'dateCreate', 'fam', 'name', 'otch', 'phoneNumber', 'pasportNumber']

	#list_filter = ['id', 'dateCreate', 'fam', 'name', 'otch', 'phoneNumber', 'pasportNumber']
	#class Meta:
               #fields = ['id', 'fam', 'name']


	#def __unicode__(self):
	#	return u'%s' % self.pk
	#fields = ['id', 'fam', 'name']

	fieldsets = (
                (u'Личные данные', {
 	 		'fields': [('fam', 'name', 'otch'),'bithday']
                }),
                (u'Анкета', {
			'fields': [('phoneNumber','pasportNumber'),]
 		}),
		(u'Доп.информация', {
 	 	 	'fields': [('dateCreate', 'dateChange'),'scoreMark']
                })
        )

	class Meta:
		#fields = ['fam', 'name', 'name']

		model = Client
		#app_label = ('Client', u'Модуль агрегатора')
		verbose_name = u'Анкета клиента'
		verbose_name_plural = u'Анкеты клиентов'

class IssueAdmin(admin.ModelAdmin):

	#list_display = ['id', 'fam', 'name','partner','credorg']

        fieldsets = (
                (u'Параметры завки',
			{'fields': (
				    ('client', 'offer', 'status'),
				    #('id','datecreate','fam','name','partner','credorg'),)
			)}
		),
                (u'Доп.информация', {
                        'fields': (('dateCreate', 'dateSend'),)
                })
        )

        def __unicode__(self):
		return u'%s' % self.pk

        class Meta:
                model = Issue
                verbose_name = u'Короткая заявка'
                verbose_name_plural = u'Короткие заявки'

class OfferAdmin(admin.ModelAdmin):

	fieldsets = (
                (u'Параметры предложения', {
                        'fields': (('offerType','offerName', 'credorg'),)
                }),
                (u'Доп.информация', {
                        'fields': (
				('minScoreMark', 'maxScoreMark'),
				('dateCreate', 'dateChange'),
				('dateRotationStart', 'dateRotationFinish'))
                })
        )

	def __unicode__(self):
		return u'%s' % self.pk

	class Meta:
		model = Offer
		verbose_name = u'Предложение'
		verbose_name_plural = u'Предложения'

admin.site.register(Client, ClientAdmin)
admin.site.register(Issue, IssueAdmin)
admin.site.register(Offer, OfferAdmin)

