#coding: utf-8

from django.db import models
from client.models import *
#from performer.models import *

from datetime import datetime

ISSUE_STATUS = (
	(0, 'NEW'),
	(1, 'SENT'),
)

# Create your models here.
class Issue(models.Model):
	NEW = 0
	SENT = 1

 	client_id = models.ForeignKey(u'Клиент:', 'Client')
        offer_id = models.ForeignKey(u'Заявка:', 'Offer')

        status = models.IntegerField(u'Статус заявки:', choices = (('NEW', NEW), ('SENT', SENT)))

	dateCreate = models.DateTimeField(u'Дата и время создания:', default=datetime.now)
	dateChange = models.DateTimeField(u'Дата и время изменения:', default=datetime.now)
	#dateRotationStart = models.DateTimeField(u'Дата и время начала ротации:', default=datetime.now)
	#dateRotationFinish = models.DateTimeField(u'Дата и время окончания ротации:', default=datetime.now)

	#offerName = models.CharField(u'Название предложения:', max_length=100)
	#offerType = models.IntegerField(u'Тип предложения:', choices=OFFER_TYPE)

	#maxScoreMark = models.FloatField(u'Максимальный скорринговый балл:', max_length=3)
	#minScoreMark = models.FloatField(u'Минимальный скорринговый балл:', max_length=3)

	class Meta:
		db_table = 'issues'
		verbose_name = u'Заявка'
		verbose_name_plural = u'Заявки'

 	def __unicode__(self):
 	 	return u'%s' % self.pk

