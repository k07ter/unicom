# coding: utf-8

from django.db import models
#from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render, render_to_response
from datetime import datetime
from unicom.models import CredOrg

OFFER_TYPE = (
                (1, u'Потреб'),
                (2, u'Ипотека'),
                (3, u'Кредит'),
                (4, u'КМСБ')
        )

"""
class Client(models.Model):

        fam  = models.CharField(u'Фамилия:', max_length=30)
        name = models.CharField(u'Имя:', max_length=20)
        otch = models.CharField(u'Отчество:', max_length=30)

        bithday = models.DateField(u'День Рождения:', default=datetime.now)

        phoneNumber = models.CharField(u'Номер телефона:', max_length=12)
        pasportNumber = models.CharField(u'Номер паспорта:', max_length=12)

        dateCreated = models.DateTimeField(u'Дата и время создания анкеты:', default=datetime.now)
        dateChanged = models.DateTimeField(u'Дата и время изменения анкеты:', default=datetime.now)

        scoreMark = models.FloatField(u'Скорринговый балл', max_length=3)

        #def show_demo(self):
        #       client = this.objects.all()
        #       return render_to_response('client/index.html')

        def __str__(self):
                return u'%s' % self.fam

        class Meta:
                db_table = 'clients'
                verbose_name = u'Клиент'
                verbose_name_plural = u'Клиенты'

# Create your models here.
class Offer(models.Model):

        offerName = models.CharField(u'Название предложения:', max_length=100)
        offerType = models.IntegerField(u'Тип предложения:', choices=OFFER_TYPE)

        maxScoreMark = models.FloatField(u'Максимальный скорринговый балл:', max_length=3)
        minScoreMark = models.FloatField(u'Минимальный скорринговый балл:', max_length=3)

        client = models.ForeignKey(Client, verbose_name=u'Кредитная организация:')

        dateCreate = models.DateTimeField(u'Дата и время создания:', auto_now_add=True, default=datetime.now)
        dateChange = models.DateTimeField(u'Дата и время изменения:', default=datetime.now)
        dateRotationStart = models.DateTimeField(u'Дата и время начала ротации:', default=datetime.now)
        dateRotationFinish = models.DateTimeField(u'Дата и время окончания ротации:', default=datetime.now)

        class Meta:
                db_table = 'offers'
                verbose_name = u'Предложение'
                verbose_name_plural = u'Предложения'

        def __str__(self):
                return  u'%s' % self.offerName

"""
# Create your models here.
class Issue(models.Model):
	NEW = 0
	SENT = 1

	client = models.ForeignKey('Client', verbose_name=u'Клиент:')
        offer = models.ForeignKey('Offer', verbose_name=u'Предложение:')

	status = models.IntegerField(u'Статус заявки:', choices=((NEW, 'NEW'), (SENT, 'SENT')), default=NEW)

	dateCreate = models.DateTimeField(u'Дата и время создания:', default=datetime.now)
	dateSend = models.DateTimeField(u'Дата и время отправки:', default=datetime.now)

	#offerName = models.CharField(u'Название предложения:', max_length=100)
	#offerType = models.IntegerField(u'Тип предложения:', choices=OFFER_TYPE)
	#maxScoreMark = models.FloatField(u'Максимальный скорринговый балл:', max_length=3)
	#minScoreMark = models.FloatField(u'Минимальный скорринговый балл:', max_length=3)

	class Meta:
		db_table = 'issues'
		verbose_name = u'Заявка'
		verbose_name_plural = u'Краткие заявки'

	def __unicode__(self):
 		return u'%s' % self.pk

# Create your models here.
class Offer(models.Model):

	offerName = models.CharField(u'Название предложения:', max_length=100)
	offerType = models.IntegerField(u'Тип предложения:', choices=OFFER_TYPE)

	maxScoreMark = models.FloatField(u'Максимальный скорринговый балл:', max_length=3)
	minScoreMark = models.FloatField(u'Минимальный скорринговый балл:', max_length=3)

	credorg = models.ForeignKey(CredOrg, verbose_name=u'Кредитная организация:')

 	dateCreate = models.DateTimeField(u'Дата и время создания:', default=datetime.now)
        dateChange = models.DateTimeField(u'Дата и время изменения:', default=datetime.now)
        dateRotationStart = models.DateTimeField(u'Дата и время начала ротации:', default=datetime.now)
        dateRotationFinish = models.DateTimeField(u'Дата и время окончания ротации:', default=datetime.now)

	class Meta:
		db_table = 'offers'
		verbose_name = u'Предложение'
		verbose_name_plural = u'Предложения'

 	def __unicode__(self):
		return  u'%s' % self.offerName

class Client(models.Model):

        fam  = models.CharField(u'Фамилия:', max_length=30)
        name = models.CharField(u'Имя:', max_length=20)
        otch = models.CharField(u'Отчество:', max_length=30)

        bithday = models.DateField(u'День Рождения:', default=datetime.now)

        phoneNumber = models.CharField(u'Номер телефона:', max_length=12)
        pasportNumber = models.CharField(u'Номер паспорта:', max_length=12)

        dateCreate = models.DateTimeField(u'Дата и время создания анкеты:', default=datetime.now)
        dateChange = models.DateTimeField(u'Дата и время изменения анкеты:', default=datetime.now)

        scoreMark = models.FloatField(u'Скорринговый балл', max_length=3)

        #def show_demo(self):
        #       client = this.objects.all()
        #       return render_to_response('client/index.html')

        def __unicode__(self):
                return u'%s' % self.fam

        class Meta:
                db_table = 'clients'
		#app_label = (Client, u'Клиенты')
                verbose_name = u'Анкета клиента'
                verbose_name_plural = u'Анкеты клиентов'

