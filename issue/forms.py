#coding: utf-8

from django.db import models
from customer.models import *
from performer.models import *

from datetime import datetime

ISSUE_STAT = (
	(1, u'Одобрено'),
	(0, u'Отказано'),
)

# Create your models here.
class Issue(models.Model):
	issuenom = models.CharField(u'Номер заявки', max_length=10)
	#customer_id = models.ForeignKey(u'Заказчик', 'Customer')
        #performer_id = models.ForeignKey(u'Исполнитель', 'Performer')
	status = models.IntegerField(u'Решение по МП', choices=ISSUE_STAT)
	date = models.DateTimeField(u'Дата решения', default=datetime.now)

	class Meta:
		verbose_name = u'Заявка'
		verbose_name_plural = u'Заявки'

class IssueForm(models.Model):
	issuenom = models.CharField(u'Номер заявки', max_length=10)
	#customer_id = models.ForeignKey(u'Заказчик', 'Customer')
        #performer_id = models.ForeignKey(u'Исполнитель', 'Performer')
	status = models.IntegerField(u'Решение по МП', choices=ISSUE_STAT)
	date = models.DateTimeField(u'Дата решения', default=datetime.now)

	class Meta:
		verbose_name = u'Заявка'
		verbose_name_plural = u'Заявки'

#class Service():
#	name = models.CharField(u'Наименование', max_length=10)
