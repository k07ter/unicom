#coding: utf-8

from django.contrib import admin
from models import Issue

# Register your models here.
class IssueAdmin(admin.ModelAdmin):
	def __unicode__(self):
		return u'%s' % 'issue'
		
	class Meta:
		model = Issue
		verbose_name = u'Заявка'
		verbose_name_plural = u'Заявки'

admin.site.register(Issue, IssueAdmin)

