#coding: utf-8

from django.contrib import admin
from models import Partner, CredOrg, SpecGroup

#from client.models import *
#from client.admin import *

# Register your models here.
class CredOrgAdmin(admin.ModelAdmin): pass

class PartnerAdmin(admin.ModelAdmin):
		
	#list_exclude = ('fam','name','email')

	class Meta:
		model = Partner
		verbose_name = u'Партнер'
		verbose_name_plural = u'Партнеры'

	def save_model(self, request, form, obj):
		return ''

class SpecGroupAdmin(admin.ModelAdmin):
	class Meta:
		model = SpecGroup
		verbose_name = u'Группа'

admin.site.register(Partner, PartnerAdmin)
admin.site.register(SpecGroup, SpecGroupAdmin)
#admin.site.register(User, UserAdmin)
admin.site.register(CredOrg, CredOrgAdmin)
#admin.site.register(Client, ClientAdmin)
#admin.site.register(Issue, IssueAdmin)
#admin.site.register(Offer, OfferAdmin)

