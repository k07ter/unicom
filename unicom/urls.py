from django.conf.urls import patterns, include, url

from django.contrib import admin

import djadmin2

#djadmin2.default.autodiscover()
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'unicom.views.home', name='home'),
    #url(r'^client/', include('client.urls')),

    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    #url(r'^api_agr/', include('api_agr.urls', namespace='api_agr')),
    url(r'^admin/', include(admin.site.urls)),
)
