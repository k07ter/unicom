#coding: utf-8

from django.db import models
#import serializers

from django.contrib.auth.models import User, Group, Permission
from api_agr.v1.models import Api_Agr
# Create your models here.

#class ApiPoint(models.Model):
#	abstract_class = True
# 	api = models.ForeignKey(ApiAgr, verbose_name)

class CredOrg(models.Model):
	'''Модель кредитной организации
	'''
 	naim = models.CharField(u'Кредитная организация', max_length=50)
	#offer = models.CharField(u'Предложение', max_length=30)
	#partner = models.ForeignKey('Partner', verbose_name=u'Партнер')
	#api = models.ForeignKey(u'API', verbose_name='API')

	def __unicode__(self):
 	 	return u'%s' % self.naim

	class Meta:
 	 	verbose_name = u'Кредитная организация'
		verbose_name_plural = 'Кредитные организации'

	def lookup(self):
		'''Списо заявок
		'''
 	 	#return self.objects.all

 	def lookup_id(self, id):
		'''Просмотр заявки
		'''
 	 	#return self.find(id).all
 	 	#print id

class Partner(User):
	'''Модель персонал
	'''
        #attestat_nomer = models.CharField(u'Номер аттестата', max_length=10)
	fam = models.CharField(u'Фамилия', max_length=30)
	fake_name = models.CharField(u'Имя', max_length=20)
	#login = models.CharField(u'Логин', max_length=10)
	#issue = models.CharField(u'Заявка', max_length=30)
	#credorg = models.ForeignKey(CredOrg, verbose_name=u'Персонал')
	#api = models.ForeignKey(ApiAgr, verbose_name='API')

 	def __unicode__(self):
 	 	return u'%s' % self.name

	#def lookup(self):
	#	'''Список предложений
	#	'''
	#	return self.objects.all
	
	#def lookup_id(self, id):
	#	return self.objects.filter(id=1)
class SpecGroup(Group):
	
	fake_name = models.CharField(u'Название', max_length=20)

	def __unicode__(self):
		return u'%s' % self.name

